package com.example.core.interfaces;


import com.example.core.base.callbacks.OnLoadPageListener;
import com.example.domain.model.FilmList;
import com.example.domain.model.ImageConfiguration;

public interface FilmAdapterInterface {

    void setFilmList(FilmList filmList);

    void setImageConfiguration(ImageConfiguration imageConfiguration);

    void setLoadPageListener(OnLoadPageListener pageListener);

    void addFilmList(FilmList filmList);
}
