package com.example.core.base.presenters;


import android.os.Bundle;
import android.support.annotation.Nullable;

public interface BasePresenterInterface {

    /**
     * This method will be executed on activity/fragment's onCreate method
     */
    void onCreate(@Nullable Bundle bundle);

    /**
     * This method will be executed on activity/fragment's onStart method
     */
    void onStart();

    /**
     * This method will be executed on activity/fragment's onStop method
     */
    void onStop();

    /**
     * This method will be executed on activity/fragment's onDestroy method
     */
    void onDestroy();
}

