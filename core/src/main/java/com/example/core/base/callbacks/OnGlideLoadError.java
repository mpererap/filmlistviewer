package com.example.core.base.callbacks;


public interface OnGlideLoadError {

    void onLoadError();
}
