package com.example.core.base.presenters;


import com.example.core.base.views.BaseView;

/**
 * The presenter will be the bridge between Activity or Fragment and an interactor.
 * Basically a presenter will communicate the results of background tasks (like a server
 * presentationRequested) that are needed to affect the UI
 */
public abstract class BasePresenter<T extends BaseView> implements BasePresenterInterface {

    protected T view;

    public BasePresenter(T view) {
        this.view = view;
    }

    /**
     * This method will be executed on activity/fragment's onDestroy method
     */
    public void onDestroy() {
        view = null;
    }
}

