package com.example.core.base.callbacks;


public interface OnLoadPageListener {

    void loadPage(int pageToLoad, int middlePosition);
}
