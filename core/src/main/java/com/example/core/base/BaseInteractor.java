package com.example.core.base;


import rx.Observable;

/**
 * Base interceptor to be implemented by all interceptors that fetches data
 */

public interface BaseInteractor<T> {

    Observable<T> execute();
}
