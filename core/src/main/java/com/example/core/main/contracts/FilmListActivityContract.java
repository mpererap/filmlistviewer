package com.example.core.main.contracts;


import com.example.core.base.presenters.BasePresenterInterface;
import com.example.core.base.views.BaseView;

public interface FilmListActivityContract {

    interface View extends BaseView {

        int getFragmentContainerId();

        void loadFilmListView(int containerId);

        void showLoading();

        void showContainer();

        void showError();
    }

    interface Presenter extends BasePresenterInterface {

    }
}