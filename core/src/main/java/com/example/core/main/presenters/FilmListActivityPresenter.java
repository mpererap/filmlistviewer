package com.example.core.main.presenters;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.core.base.presenters.BasePresenter;
import com.example.core.main.contracts.FilmListActivityContract;
import com.example.core.main.interactors.ConfigurationInteractor;
import com.example.domain.model.Configuration;
import com.example.domain.model.ErrorDetails;
import com.example.domain.repositories.RepositorySubscriber;
import com.example.domain.repositories.RepositoryUtils;

import rx.Subscription;

public class FilmListActivityPresenter extends BasePresenter<FilmListActivityContract.View>
        implements FilmListActivityContract.Presenter {

    private ConfigurationInteractor configurationInteractor;
    private Subscription configurationSubscription;

    public FilmListActivityPresenter(FilmListActivityContract.View view,
                                     final ConfigurationInteractor configurationInteractor) {
        super(view);
        this.configurationInteractor = configurationInteractor;
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        loadConfiguration();
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onDestroy() {
        RepositoryUtils.cancel(configurationSubscription);
        configurationInteractor = null;
        configurationSubscription = null;
        super.onDestroy();
    }

    private void showError() {
        if (view != null) {
            view.showError();
        }
    }

    /**
     * Execute ConfigurationInteractor in order to retrieve the app configuration
     */
    private void loadConfiguration() {
        view.showLoading();
        // Do not execute interceptor if it is already in progress
        if (configurationSubscription != null || configurationInteractor == null) {
            return;
        }

        configurationSubscription = RepositoryUtils.getSubscription(
                configurationInteractor.execute(), getConfigurationSubscriber());
    }

    private RepositorySubscriber<Configuration> getConfigurationSubscriber() {
        return new RepositorySubscriber<Configuration>() {
            @Override
            public void onError(ErrorDetails errorDetails) {
                configurationSubscription = null;
                if (view == null) {
                    return;
                }
                showError();
            }

            @Override
            public void onNext(Configuration configuration) {
                configurationSubscription = null;
                if (view == null) {
                    return;
                }

                view.showContainer();
                view.loadFilmListView(view.getFragmentContainerId());
            }
        };
    }
}