package com.example.core.main.interactors;


import com.example.core.base.BaseInteractor;
import com.example.domain.model.FilmList;
import com.example.domain.repositories.Repository;

import rx.Observable;

public class SearchFilmInteractor implements BaseInteractor<FilmList> {

    private final Repository repository;

    private int page;

    private String searchTerm;

    public SearchFilmInteractor(final Repository repository) {
        this.repository = repository;
    }

    public void setPage(int page) {
        this.page = page == 0 ? 1 : page;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    @Override
    public Observable<FilmList> execute() {
        return repository.searchFilm(page, searchTerm);
    }
}