package com.example.core.main.interactors;


import rx.Observable;

import com.example.core.base.BaseInteractor;
import com.example.domain.model.FilmList;
import com.example.domain.repositories.Repository;

public class PopularInteractor implements BaseInteractor<FilmList> {

    private final Repository repository;

    private int page;

    public PopularInteractor(final Repository repository) {
        this.repository = repository;
    }

    public void setPage(int page) {
        this.page = page == 0 ? 1 : page;
    }

    @Override
    public Observable<FilmList> execute() {
        return repository.getPopularFilms(page);
    }
}
