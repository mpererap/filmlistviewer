package com.example.core.main.interactors;


import com.example.core.base.BaseInteractor;
import com.example.domain.model.Configuration;
import com.example.domain.repositories.Repository;

import rx.Observable;

public class ConfigurationInteractor implements BaseInteractor<Configuration> {

    private final Repository repository;

    public ConfigurationInteractor(final Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<Configuration> execute() {
        return repository.getConfiguration();
    }
}
