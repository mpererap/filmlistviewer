package com.example.core.main.presenters;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.core.base.presenters.BasePresenter;
import com.example.core.interfaces.FilmAdapterInterface;
import com.example.core.main.contracts.FilmListFragmentContract;
import com.example.core.main.interactors.ConfigurationInteractor;
import com.example.core.main.interactors.PopularInteractor;
import com.example.core.main.interactors.SearchFilmInteractor;
import com.example.domain.model.Configuration;
import com.example.domain.model.ErrorDetails;
import com.example.domain.model.FilmList;
import com.example.domain.repositories.RepositorySubscriber;
import com.example.domain.repositories.RepositoryUtils;

import rx.Subscription;

public class FilmListFragmentPresenter extends BasePresenter<FilmListFragmentContract.View> implements FilmListFragmentContract.Presenter {

    private ConfigurationInteractor configurationInteractor;
    private PopularInteractor popularInteractor;
    private SearchFilmInteractor searchFilmInteractor;
    private Subscription configurationSubscription;
    private Subscription popularSubscription;
    private Subscription searchSubscription;
    private FilmAdapterInterface adapter;

    private int page;
    private int middleList;
    private boolean firstTime;
    private String searchTerm;

    public FilmListFragmentPresenter(FilmListFragmentContract.View view,
                                     final ConfigurationInteractor configurationInteractor,
                                     final PopularInteractor popularInteractor,
                                     final SearchFilmInteractor searchFilmInteractor,
                                     final FilmAdapterInterface adapter) {
        super(view);
        this.configurationInteractor = configurationInteractor;
        this.popularInteractor = popularInteractor;
        this.searchFilmInteractor = searchFilmInteractor;
        this.adapter = adapter;

        firstTime = true;
        middleList = 0;
        page = 1;
        searchTerm = "";
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        if (adapter != null) {
            adapter.setLoadPageListener((pageToLoad, middlePosition) -> {
                page = pageToLoad;
                middleList = middlePosition;
                if (searchTerm.isEmpty()) {
                    loadPopularFilms();
                } else {
                    searchFilms();
                }
            });
        }
        loadConfiguration();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        RepositoryUtils.cancel(configurationSubscription);
        RepositoryUtils.cancel(popularSubscription);
        RepositoryUtils.cancel(searchSubscription);
        configurationInteractor = null;
        popularInteractor = null;
        searchFilmInteractor = null;
        configurationSubscription = null;
        popularSubscription = null;
        searchSubscription = null;
        adapter = null;
        super.onDestroy();
    }

    @Override
    public void searchFilm(String searchTerm) {
        this.searchTerm = searchTerm;
        RepositoryUtils.cancel(popularSubscription);
        RepositoryUtils.cancel(searchSubscription);
        page = 0;
        firstTime = true;

        if (configurationSubscription == null) {
            if (this.searchTerm.isEmpty()) {
                loadPopularFilms();
            } else {
                searchFilms();
            }
        }
    }

    private void showError() {
        if (view != null) {
            view.showError();
        }
    }

    private void loadConfiguration() {
        view.showLoading();

        if (configurationSubscription != null || configurationInteractor == null) {
            return;
        }

        configurationSubscription = RepositoryUtils.getSubscription(
                configurationInteractor.execute(), getConfigurationSubscriber());
    }

    private void loadPopularFilms() {
        view.showLoading();
        // Do not execute interceptor if it is already in progress
        if (popularSubscription != null || popularInteractor == null) {
            return;
        }

        popularInteractor.setPage(page);

        popularSubscription = RepositoryUtils.getSubscription(
                popularInteractor.execute(), getPopularSubscriber());
    }

    private void searchFilms() {
        view.showLoading();
        // Do not execute interceptor if it is already in progress
        if (searchSubscription != null || searchFilmInteractor == null) {
            return;
        }

        searchFilmInteractor.setPage(page);
        searchFilmInteractor.setSearchTerm(searchTerm);

        searchSubscription = RepositoryUtils.getSubscription(
                searchFilmInteractor.execute(), getSearchFilmSubscriber());
    }

    private RepositorySubscriber<Configuration> getConfigurationSubscriber() {
        return new RepositorySubscriber<Configuration>() {
            @Override
            public void onError(ErrorDetails errorDetails) {
                configurationSubscription = null;
                if (view == null) {
                    return;
                }
                showError();
            }

            @Override
            public void onNext(Configuration configuration) {
                configurationSubscription = null;
                if (adapter == null && configuration == null) {
                    showError();
                    return;
                }

                adapter.setImageConfiguration(configuration.getImageConfiguration());
                if (searchTerm.isEmpty()) {
                    loadPopularFilms();
                } else {
                    searchFilms();
                }
            }
        };
    }

    private RepositorySubscriber<FilmList> getPopularSubscriber() {
        return new RepositorySubscriber<FilmList>() {
            @Override
            public void onError(ErrorDetails errorDetails) {
                popularSubscription = null;
                if (view == null) {
                    return;
                }
                showError();
            }

            @Override
            public void onNext(FilmList filmList) {
                popularSubscription = null;
                if (view == null) {
                    return;
                }

                if (firstTime) {
                    adapter.setFilmList(filmList);
                    firstTime = false;
                } else {
                    view.goToPosition(middleList);
                    adapter.addFilmList(filmList);
                }

                view.showContainer();
            }
        };
    }

    private RepositorySubscriber<FilmList> getSearchFilmSubscriber() {
        return new RepositorySubscriber<FilmList>() {
            @Override
            public void onError(ErrorDetails errorDetails) {
                searchSubscription = null;
                if (view == null) {
                    return;
                }
                showError();
            }

            @Override
            public void onNext(FilmList filmList) {
                searchSubscription = null;
                if (view == null) {
                    return;
                }

                if (firstTime) {
                    adapter.setFilmList(filmList);
                    firstTime = false;
                } else {
                    view.goToPosition(middleList);
                    adapter.addFilmList(filmList);
                }

                view.showContainer();
            }
        };
    }
}
