package com.example.core.main.contracts;


import com.example.core.base.presenters.BasePresenterInterface;
import com.example.core.base.views.BaseView;

public interface FilmListFragmentContract {

    interface View extends BaseView {

        void showError();

        void showLoading();

        void showContainer();

        void goToPosition(int i);
    }

    interface Presenter extends BasePresenterInterface {

        void searchFilm(String searchTerm);
    }
}