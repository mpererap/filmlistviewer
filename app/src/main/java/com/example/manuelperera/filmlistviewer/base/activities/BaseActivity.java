package com.example.manuelperera.filmlistviewer.base.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.example.core.base.presenters.BasePresenterInterface;
import com.example.manuelperera.filmlistviewer.App;
import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;

import java.util.List;

/**
 * Simple base activity. Extends {@link BasePresenterInterface} and
 * {@link android.support.v7.app.AppCompatCallback}
 */
public abstract class BaseActivity<T extends BasePresenterInterface> extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResourceId());

        injectDependencies();
        initViews();

        final Bundle bundle;
        if (savedInstanceState == null) {
            bundle = getIntent().getExtras();
        } else {
            bundle = savedInstanceState;
        }

        T presenter = getPresenter();

        if (presenter != null) {
            presenter.onCreate(bundle);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(
                () -> {
                    boolean canBack = getSupportFragmentManager().getBackStackEntryCount() > 0;
                    ActionBar actionBar = getSupportActionBar();
                    if (actionBar != null && actionBar.isShowing()) {
                        actionBar.setDisplayHomeAsUpEnabled(canBack);
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();

        T presenter = getPresenter();

        if (presenter != null) {
            presenter.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        T presenter = getPresenter();

        if (presenter != null) {
            presenter.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        T presenter = getPresenter();

        if (presenter != null) {
            presenter.onDestroy();
        }

        destroy();
    }

    @Override
    public void onBackPressed() {
        if (recursivelyPopBackStack(getSupportFragmentManager())) {
            return;
        }
        super.onBackPressed();
    }

    /**
     * Pops recursively the last {@link Fragment} added to any back stack. No matter if this last
     * {@link Fragment} was added to the Activity itself or to any of the possibly nested Fragments.
     * <p>
     * This is a mandatory workaround as Google doesn't fixed it yet (maybe they will never fix it):
     * https://code.google.com/p/android/issues/detail?id=40323
     * <p>
     * This solution is based on this StackOverflow answer:
     * http://stackoverflow.com/a/24176614/2969811
     *
     * @param fragmentManager The {@link FragmentManager} used to pop the back stack
     * @return true if any {@link Fragment} was popped, false otherwise
     */
    private boolean recursivelyPopBackStack(FragmentManager fragmentManager) {
        if (fragmentManager == null) {
            return false;
        }

        List<Fragment> fragmentList = fragmentManager.getFragments();
        if (fragmentList != null && !fragmentList.isEmpty()) {
            Fragment fragment;
            for (int i = fragmentList.size() - 1; i >= 0; i--) {
                fragment = fragmentList.get(i);
                if (fragment == null) {
                    continue;
                }
                if (fragment.isVisible() && recursivelyPopBackStack(
                        fragment.getChildFragmentManager())) {
                    return true;
                }
            }
        }

        return fragmentManager.getBackStackEntryCount() > 0
                && fragmentManager.popBackStackImmediate();
    }

    /**
     * Setup the object graph and inject the dependencies needed on this activity.
     */
    private void injectDependencies() {
        setupComponent(App.getApp(this).getApplicationComponent());
    }

    /**
     * Specify the layout of the fragment to be inflated in the {@link BaseActivity#onCreate(Bundle)}
     */
    protected abstract int getLayoutResourceId();

    /**
     * Initialize all views
     */
    protected abstract void initViews();

    /**
     * Destroy all objects
     */
    protected abstract void destroy();

    /**
     * @return The presenter attached to the activity. This must extends from {@link BasePresenterInterface}
     */
    protected abstract T getPresenter();

    /**
     * Set up dagger 2 component
     *
     * @param appComponent Application component
     */
    protected abstract void setupComponent(ApplicationComponent appComponent);
}
