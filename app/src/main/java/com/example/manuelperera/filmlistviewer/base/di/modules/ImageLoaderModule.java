package com.example.manuelperera.filmlistviewer.base.di.modules;


import com.example.manuelperera.filmlistviewer.base.di.scopes.PerApplication;
import com.example.manuelperera.filmlistviewer.base.helpers.ImageLoader;

import dagger.Module;
import dagger.Provides;

@Module
public class ImageLoaderModule {

    @Provides
    @PerApplication
    ImageLoader providesImageLoader() {
        return new ImageLoader();
    }
}
