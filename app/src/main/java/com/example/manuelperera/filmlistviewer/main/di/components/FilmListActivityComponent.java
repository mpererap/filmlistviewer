package com.example.manuelperera.filmlistviewer.main.di.components;


import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;
import com.example.manuelperera.filmlistviewer.base.di.modules.RepositoryModule;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerActivity;
import com.example.manuelperera.filmlistviewer.main.activities.FilmListActivity;
import com.example.manuelperera.filmlistviewer.main.di.modules.FilmListActivityModule;

import dagger.Component;

@PerActivity
@Component(dependencies = { ApplicationComponent.class },
        modules = {
                FilmListActivityModule.class
        }
)

public interface FilmListActivityComponent {

    void inject(FilmListActivity filmListActivity);
}
