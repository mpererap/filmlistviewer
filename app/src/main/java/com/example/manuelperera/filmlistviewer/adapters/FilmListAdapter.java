package com.example.manuelperera.filmlistviewer.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.core.base.callbacks.OnLoadPageListener;
import com.example.core.interfaces.FilmAdapterInterface;
import com.example.domain.model.Film;
import com.example.domain.model.FilmList;
import com.example.domain.model.ImageConfiguration;
import com.example.manuelperera.filmlistviewer.R;
import com.example.manuelperera.filmlistviewer.base.helpers.ImageLoader;
import com.example.manuelperera.filmlistviewer.viewholders.EmptyListViewHolder;
import com.example.manuelperera.filmlistviewer.viewholders.FilmViewHolder;
import com.example.manuelperera.filmlistviewer.viewholders.LoadingViewHolder;

import java.util.ArrayList;
import java.util.List;

public class FilmListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements FilmAdapterInterface {

    private static int MAX_BOOK_PAGES = 2;

    private List<FilmList> filmBook;

    private int currentPage;
    private int lastPage;
    private int minLoadedPage;
    private int topLoadedPage;
    private int currentFilmCount;

    private ImageLoader imageLoader;
    private OnLoadPageListener pageListener;

    protected enum Type {
        PLACEHOLDER(0),
        HEADER(1),
        FOOTER(2),
        FILM(3);

        private final int value;

        Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Type getByValue(int value) {
            for (Type type : Type.values()) {
                if (type != null && type.getValue() == value) {
                    return type;
                }
            }
            return null;
        }
    }

    public FilmListAdapter(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPlaceholder()) {
            return Type.PLACEHOLDER.getValue();
        } else if (isHeader(position)) {
            return Type.HEADER.getValue();
        } else if (isFooter(position)) {
            return Type.FOOTER.getValue();
        }

        return Type.FILM.getValue();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final Type type = Type.getByValue(viewType);
        if (type == null) {
            return null;
        }
        RecyclerView.ViewHolder viewHolder;
        switch (type) {
            case HEADER:
                viewHolder = onCreateHeaderViewHolder(parent);
                break;
            case FOOTER:
                viewHolder = onCreateFooterViewHolder(parent);
                break;
            case PLACEHOLDER:
                viewHolder = onCreatePlaceholderViewHolder(parent);
                break;
            case FILM:
                viewHolder = onCreateFilmViewHolder(parent);
                break;
            default:
                viewHolder = onCreatePlaceholderViewHolder(parent);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Film film = getItem(position);
        if (holder instanceof FilmViewHolder && film != null) {
            ((FilmViewHolder) holder).setValue(film, imageLoader);
        } else if (holder instanceof LoadingViewHolder && isLastOne(position)) {
            int toLoad = topLoadedPage == lastPage ? lastPage : topLoadedPage + 1;
            int positionToMove = filmBook.size() > 1 ? getItemCount() / 2
                    : getItemCount() - 1;
            ((LoadingViewHolder) holder).setValue(toLoad, positionToMove, pageListener);
        } else if (holder instanceof LoadingViewHolder && !isLastOne(position)) {
            int toLoad = minLoadedPage == 1 ? 1 : minLoadedPage - 1;
            int positionToMove = filmBook.size() > 1 ? getItemCount() / 2
                    : getItemCount() - 1;
            ((LoadingViewHolder) holder).setValue(toLoad, positionToMove, pageListener);
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;

        // Placeholder
        if (currentFilmCount == 0) {
            return 1;
        }

        if (minLoadedPage > 1) {
            count++;
        }

        if (topLoadedPage < lastPage) {
            count++;
        }

        return count + currentFilmCount;
    }

    public void addFilmList(FilmList filmList) {
        if (filmBook == null) {
            filmBook = new ArrayList<>();
        }

        if(filmBook.isEmpty()) {
            setCleanPage(filmList);
        }

        if (filmBook.size() < MAX_BOOK_PAGES) {

            addToBook(filmList);

        } else {

            makeSpaceForPage(filmList);
            addToBook(filmList);
        }

        notifyDataSetChanged();
    }

    public void setFilmList(FilmList filmList) {
        if (filmBook == null) {
            filmBook = new ArrayList<>();
        }

        setCleanPage(filmList);

        notifyDataSetChanged();
    }

    @Override
    public void setImageConfiguration(ImageConfiguration imageConfiguration) {
        if (imageLoader != null) {
            imageLoader.setImageConfiguration(imageConfiguration);
        }
    }

    @Override
    public void setLoadPageListener(OnLoadPageListener pageListener)  {
        this.pageListener = pageListener;
    }

    private void setCleanPage(FilmList filmList) {

        if (filmBook == null || filmList == null || filmList.getTotalResults() == 0) {
            return;
        }

        filmBook.clear();

        filmBook.add(filmList);
        topLoadedPage = filmList.getPage();
        minLoadedPage = filmList.getPage();
        currentPage = filmList.getPage();
        lastPage = filmList.getTotalPages();

        List<Film> films = filmList.getResults();
        currentFilmCount = films == null ? 0 : films.size();
    }

    private void addToBook(FilmList filmList) {
        if (filmBook == null || filmList == null) {
            return;
        }

        if (filmBook.isEmpty()) {
            setCleanPage(filmList);
            return;
        }

        currentPage = filmList.getPage();

        FilmList firstFilmList = filmBook.get(0);
        int firstPage = firstFilmList == null ? 0 : firstFilmList.getPage();
        if (firstPage < filmList.getPage()) {
            filmBook.add(filmList);
            topLoadedPage = filmList.getPage();
        } else {
            filmBook.add(0, filmList);
            minLoadedPage = filmList.getPage();
        }

        List<Film> filmResults = filmList.getResults();
        currentFilmCount += filmResults == null ? 0 : filmResults.size();
    }

    private void makeSpaceForPage(FilmList filmList) {
        if (filmBook == null || filmList == null || filmBook.isEmpty()
                || filmBook.size() < MAX_BOOK_PAGES) {
            return;
        }

        FilmList firstFilmList = filmBook.get(0);
        int firstPage = firstFilmList == null ? 0 : firstFilmList.getPage();
        if (firstPage < filmList.getPage()) {
            FilmList removed = filmBook.remove(0);
            List<Film> filmResults = removed == null ? null : removed.getResults();
            if (filmResults != null) {
                currentFilmCount -= filmResults.size();
            }

            if(filmBook.size() == 0) {
                minLoadedPage = 0;
            } else {
                FilmList newFirst = filmBook.get(0);
                minLoadedPage = newFirst == null ? 0 : newFirst.getPage();
            }
        } else {
            int bookSize = filmBook.size();
            FilmList removed = filmBook.remove(bookSize - 1);
            List<Film> filmResults = removed == null ? null : removed.getResults();
            if (filmResults != null) {
                currentFilmCount -= filmResults.size();
            }

            if(filmBook.size() == 0) {
                topLoadedPage = 0;
            } else {
                FilmList newLast = filmBook.get(filmBook.size() - 1);
                topLoadedPage = newLast == null ? 0 : newLast.getPage();
            }

        }
    }

    private boolean isLastOne(int position) {
        return position == getItemCount() - 1;
    }

    private boolean isPlaceholder() {
        return currentFilmCount == 0 && getItemCount() == 1;
    }

    private boolean isHeader(int position) {
        return minLoadedPage > 1 && position == 0;
    }

    private boolean isFooter(int position) {
        return topLoadedPage < lastPage && position == getItemCount() - 1;
    }

    private RecyclerView.ViewHolder onCreateFilmViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ho_film,
                parent, false);
        return new FilmViewHolder(view);
    }

    private RecyclerView.ViewHolder onCreatePlaceholderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ho_empty_list,
                parent, false);
        return new EmptyListViewHolder(view);
    }

    private RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ho_load,
                parent, false);
        return new LoadingViewHolder(view);
    }

    private RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ho_load,
                parent, false);
        return new LoadingViewHolder(view);
    }

    private Film getItem(int position) {

        if (filmBook == null || filmBook.isEmpty()) {
            return null;
        }

        int realPosition = 0;
        if (topLoadedPage > MAX_BOOK_PAGES) {
            realPosition--;
        }

        realPosition += position;

        for (FilmList filmList : filmBook) {
            List<Film> films = filmList == null ? null : filmList.getResults();

            if (films == null) {
                continue;
            }

            int maxPosition = films.size();

            if(realPosition < maxPosition && realPosition >= 0) {
                return films.get(realPosition);
            } else {
                realPosition -= maxPosition;
            }
        }

        return null;
    }
}
