package com.example.manuelperera.filmlistviewer.main.fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.example.core.main.contracts.FilmListFragmentContract;
import com.example.manuelperera.filmlistviewer.R;
import com.example.manuelperera.filmlistviewer.adapters.FilmListAdapter;
import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;
import com.example.manuelperera.filmlistviewer.base.fragments.BaseFragment;
import com.example.manuelperera.filmlistviewer.base.helpers.ImageLoader;
import com.example.manuelperera.filmlistviewer.main.di.components.DaggerFilmListFragmentComponent;
import com.example.manuelperera.filmlistviewer.main.di.modules.FilmListFragmentModule;

import javax.inject.Inject;

import butterknife.BindView;

public class FilmListFragment extends BaseFragment<FilmListFragmentContract.Presenter> implements FilmListFragmentContract.View {

    private static final String TAG = FilmListFragment.class.getSimpleName();

    @Inject
    protected FilmListFragmentContract.Presenter presenter;
    @Inject
    protected ImageLoader imageLoader;
    @Inject
    protected FilmListAdapter adapter;

    @BindView(R.id.loading)
    protected View loadingView;
    @BindView(R.id.error)
    protected View errorView;
    @BindView(R.id.view)
    protected View fragmentView;
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;
    @BindView(R.id.search_bar)
    protected EditText searchBar;

    public FilmListFragment() {
        super(TAG);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fr_film_list;
    }

    @Override
    protected FilmListFragmentContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected void setupComponent(ApplicationComponent applicationComponent) {

        DaggerFilmListFragmentComponent.builder()
                .applicationComponent(applicationComponent)
                .filmListFragmentModule(new FilmListFragmentModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void initViews(View rootView) {
        setupRecyclerView();
        setupSearchBar();
    }

    @Override
    protected void destroyViews() {

    }

    @Override
    protected void destroy() {

    }

    @Override
    public void showError() {
        hideFragmentView();
        hideLoadingView();
        showErrorView();
    }

    @Override
    public void showLoading() {
        hideErrorView();
        hideFragmentView();
        showLoadingView();
    }

    @Override
    public void showContainer() {
        hideErrorView();
        hideLoadingView();
        showFragmentView();
    }

    @Override
    public void goToPosition(int position) {
        if (recyclerView != null) {
            recyclerView.scrollToPosition(position);
        }
    }

    private void setupRecyclerView() {
        if (recyclerView == null) {
            return;
        }
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }

    private void setupSearchBar() {

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (presenter != null) {
                    presenter.searchFilm(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void hideErrorView() {
        errorView.setVisibility(View.GONE);
    }

    private void showErrorView() {
        errorView.setVisibility(View.VISIBLE);
    }

    private void hideFragmentView() {
        fragmentView.setVisibility(View.GONE);
    }

    private void showFragmentView() {
        fragmentView.setVisibility(View.VISIBLE);
    }

    private void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }

    private void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }
}
