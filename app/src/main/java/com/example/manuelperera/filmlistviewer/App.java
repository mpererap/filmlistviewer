package com.example.manuelperera.filmlistviewer;

import android.app.Application;
import android.content.Context;

import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;
import com.example.manuelperera.filmlistviewer.base.di.components.DaggerApplicationComponent;
import com.example.manuelperera.filmlistviewer.base.di.modules.ApplicationModule;

/**
 * Application class of the project
 */
public class App extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize application
        initApp();
    }

    /**
     * Initialize application
     */
    private void initApp() {
        // Set up application injector
        setupInjector();
    }

    /**
     * Setup dagger 2 injector
     */
    private void setupInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static App getApp(Context context) {
        return (App) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}