package com.example.manuelperera.filmlistviewer.viewholders;


import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EmptyListViewHolder extends RecyclerView.ViewHolder {

    public EmptyListViewHolder(View itemView) {
        super(itemView);
    }
}
