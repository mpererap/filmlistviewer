package com.example.manuelperera.filmlistviewer.base.di.components;


import android.app.Application;
import android.content.Context;

import com.example.core.main.interactors.ConfigurationInteractor;
import com.example.domain.model.Configuration;
import com.example.domain.repositories.Repository;
import com.example.manuelperera.filmlistviewer.base.di.modules.ApplicationModule;
import com.example.manuelperera.filmlistviewer.base.di.modules.ConnectivityHelperModule;
import com.example.manuelperera.filmlistviewer.base.di.modules.ImageLoaderModule;
import com.example.manuelperera.filmlistviewer.base.di.modules.RepositoryModule;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerApplication;
import com.example.manuelperera.filmlistviewer.base.helpers.ImageLoader;
import com.example.utils.ConnectivityHelper;

import dagger.Component;

@PerApplication
@Component(modules = {ApplicationModule.class,
        RepositoryModule.class,
        ConnectivityHelperModule.class,
        ImageLoaderModule.class
})
public interface ApplicationComponent {

    Application getApplication();

    Context getContext();

    Repository getRepository();

    ConfigurationInteractor getConfigurationInteractor();

    ConnectivityHelper getConnectivityHelper();

    ImageLoader getImageLoader();
}

