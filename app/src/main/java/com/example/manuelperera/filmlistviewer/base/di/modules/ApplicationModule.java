package com.example.manuelperera.filmlistviewer.base.di.modules;


import android.app.Application;
import android.content.Context;

import com.example.manuelperera.filmlistviewer.App;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerApplication;

import dagger.Module;
import dagger.Provides;

/**
 * Module that provides dependencies for application
 * Must be injected in main app component
 */
@Module
public class ApplicationModule {

    private final App application;

    public ApplicationModule(App application) {
        this.application = application;
    }

    @Provides
    @PerApplication
    Application provideApplication() {
        return application;
    }

    @Provides
    @PerApplication
    Context provideApplicationContext() {
        return application;
    }
}
