package com.example.manuelperera.filmlistviewer.base.di.modules;


import android.content.Context;

import com.example.manuelperera.filmlistviewer.base.di.scopes.PerApplication;
import com.example.utils.ConnectivityHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class ConnectivityHelperModule {

    @Provides
    @PerApplication
    ConnectivityHelper providesConnectivityHelperModule(Context context) {
        return new ConnectivityHelper(context);
    }
}