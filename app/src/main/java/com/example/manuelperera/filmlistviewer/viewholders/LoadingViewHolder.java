package com.example.manuelperera.filmlistviewer.viewholders;


import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.core.base.callbacks.OnLoadPageListener;

public class LoadingViewHolder extends RecyclerView.ViewHolder {

    int pageToLoad;

    public LoadingViewHolder(View itemView) {
        super(itemView);
    }

    public void setValue(int pageToLoad, int middlePosition, OnLoadPageListener listener) {
        this.pageToLoad = pageToLoad;

        if (listener != null) {
            listener.loadPage(pageToLoad, middlePosition);
        }
    }
}
