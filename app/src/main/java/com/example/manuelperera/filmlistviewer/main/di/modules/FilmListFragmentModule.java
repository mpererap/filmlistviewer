package com.example.manuelperera.filmlistviewer.main.di.modules;

import com.example.core.main.contracts.FilmListFragmentContract;
import com.example.core.main.interactors.ConfigurationInteractor;
import com.example.core.main.interactors.PopularInteractor;
import com.example.core.main.interactors.SearchFilmInteractor;
import com.example.core.main.presenters.FilmListFragmentPresenter;
import com.example.domain.repositories.Repository;
import com.example.manuelperera.filmlistviewer.adapters.FilmListAdapter;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerFragment;
import com.example.manuelperera.filmlistviewer.base.helpers.ImageLoader;

import dagger.Module;
import dagger.Provides;

@Module
public class FilmListFragmentModule {

    private final FilmListFragmentContract.View view;

    public FilmListFragmentModule(FilmListFragmentContract.View view) {
        this.view = view;
    }

    @PerFragment
    @Provides
    FilmListFragmentContract.Presenter provideFilmListFragmentPresenter(
            final ConfigurationInteractor configurationInteractor,
            final PopularInteractor popularInteractor,
            final SearchFilmInteractor searchFilmInteractor, FilmListAdapter adapter) {
        return new FilmListFragmentPresenter(view, configurationInteractor, popularInteractor,
                searchFilmInteractor, adapter);
    }

    @PerFragment
    @Provides
    PopularInteractor providePopularInteractor(final Repository repository) {
        return new PopularInteractor(repository);
    }

    @PerFragment
    @Provides
    SearchFilmInteractor provideSearchFilmInteractor(final Repository repository) {
        return new SearchFilmInteractor(repository);
    }

    @PerFragment
    @Provides
    FilmListAdapter provideFilmListAdapter(ImageLoader imageLoader) {
        return new FilmListAdapter(imageLoader);
    }
}
