package com.example.manuelperera.filmlistviewer.main.di.components;

import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerFragment;
import com.example.manuelperera.filmlistviewer.main.activities.FilmListActivity;
import com.example.manuelperera.filmlistviewer.main.di.modules.FilmListActivityModule;
import com.example.manuelperera.filmlistviewer.main.di.modules.FilmListFragmentModule;
import com.example.manuelperera.filmlistviewer.main.fragments.FilmListFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = {ApplicationComponent.class},
        modules = {
            FilmListFragmentModule.class
        }
)

public interface FilmListFragmentComponent {

    void inject(FilmListFragment fragment);
}
