package com.example.manuelperera.filmlistviewer.main.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ViewSwitcher;

import com.example.core.main.contracts.FilmListActivityContract;
import com.example.manuelperera.filmlistviewer.R;
import com.example.manuelperera.filmlistviewer.base.activities.BaseActivity;
import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;
import com.example.manuelperera.filmlistviewer.base.fragments.BaseFragment;
import com.example.manuelperera.filmlistviewer.main.di.components.DaggerFilmListActivityComponent;
import com.example.manuelperera.filmlistviewer.main.di.modules.FilmListActivityModule;
import com.example.utils.NavigationUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmListActivity extends BaseActivity<FilmListActivityContract.Presenter> implements FilmListActivityContract.View {

    private static final int FRAGMENT_CONTAINER = R.id.container;

    @Inject
    protected FilmListActivityContract.Presenter presenter;
    @Inject
    protected BaseFragment fragment;

    @BindView(R.id.loading)
    protected View loadingView;
    @BindView(R.id.error)
    protected View errorView;
    @BindView(R.id.view)
    protected View fragmentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.film_list_activity_title));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.ac_film_list;
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected void destroy() {

    }

    @Override
    protected FilmListActivityContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected void setupComponent(ApplicationComponent appComponent) {

        DaggerFilmListActivityComponent.builder()
                .applicationComponent(appComponent)
                .filmListActivityModule(new FilmListActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int getFragmentContainerId() {
        return FRAGMENT_CONTAINER;
    }

    @Override
    public void loadFilmListView(int containerId) {

        if (fragment == null) {
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        NavigationUtils.navigateToFragment(false, fragmentManager, getFragmentContainerId(), fragment,
                fragment.getTAG(), null);
    }

    @Override
    public void showLoading() {
        hideErrorView();
        hideFragmentView();
        showLoadingView();
    }

    @Override
    public void showContainer() {
        hideErrorView();
        hideLoadingView();
        showFragmentView();
    }

    @Override
    public void showError() {
        hideFragmentView();
        hideLoadingView();
        showErrorView();
    }

    private void hideErrorView() {
        errorView.setVisibility(View.GONE);
    }

    private void showErrorView() {
        errorView.setVisibility(View.VISIBLE);
    }

    private void hideFragmentView() {
        fragmentView.setVisibility(View.GONE);
    }

    private void showFragmentView() {
        fragmentView.setVisibility(View.VISIBLE);
    }

    private void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }

    private void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }
}
