package com.example.manuelperera.filmlistviewer.base.helpers;


import android.graphics.Bitmap;
import android.text.TextUtils;

import com.example.core.base.callbacks.OnGlideLoadError;
import com.example.domain.model.ImageConfiguration;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DisplayImageSettings {

    private static final int PLACEHOLDER_IMAGE_RES_ID = android.R.color.transparent;
    private static final int DEFAULT_ANIMATION_TIME = (int) TimeUnit.MILLISECONDS.toMillis(250);

    private String imageUrl;
    private Bitmap localImageBitmap;
    private int loadingResourceId;
    private boolean showAnimation;
    private int animationTime;
    private ImageShape mImageShape;
    private OnGlideLoadError glideLoadError;
    private String imageReference;

    public enum ImageShape {
        CIRCLE,
        SQUARE
    }

    public DisplayImageSettings() {
        imageUrl = "";
        localImageBitmap = null;
        loadingResourceId = PLACEHOLDER_IMAGE_RES_ID;
        showAnimation = true;
        animationTime = DEFAULT_ANIMATION_TIME;
        mImageShape = ImageShape.SQUARE;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? "" : imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Bitmap getLocalImageBitmap() {
        return localImageBitmap;
    }

    public void setLocalImageBitmap(Bitmap localImageBitmap) {
        this.localImageBitmap = localImageBitmap;
    }

    public boolean hasLocalImageBitmap() {
        return localImageBitmap != null;
    }

    public DisplayImageSettings setLoadingResourceId(int loadingResourceId) {
        this.loadingResourceId = loadingResourceId > 0 ? loadingResourceId
                : PLACEHOLDER_IMAGE_RES_ID;
        return this;
    }

    public int getLoadingResourceId() {
        return loadingResourceId;
    }

    public DisplayImageSettings setShowAnimation(boolean showAnimation) {
        this.showAnimation = showAnimation;
        return this;
    }

    public boolean mustShowAnimation() {
        return showAnimation;
    }

    public DisplayImageSettings setAnimationTime(int animationTime) {
        this.animationTime = animationTime >= 0 ? animationTime : DEFAULT_ANIMATION_TIME;
        return this;
    }

    public int getAnimationTime() {
        return animationTime;
    }

    public void setImageShape(ImageShape imageShape) {
        this.mImageShape = imageShape;
    }

    public boolean isCircularImage() {
        return this.mImageShape == ImageShape.CIRCLE;
    }

    public void setGlideLoadError(OnGlideLoadError glideLoadError) {
        this.glideLoadError = glideLoadError;
    }

    public void executeErrorCallback() {
        if (glideLoadError != null) {
            glideLoadError.onLoadError();
        }
    }

    public void setImageReference(String imageReference) {
        this.imageReference = imageReference;
    }

    public String getImageReference() {
        return imageReference;
    }

    public boolean hasImageReference() {
        return !TextUtils.isEmpty(imageReference);
    }
}
