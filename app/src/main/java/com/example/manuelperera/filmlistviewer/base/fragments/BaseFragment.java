package com.example.manuelperera.filmlistviewer.base.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.core.base.presenters.BasePresenterInterface;
import com.example.manuelperera.filmlistviewer.App;
import com.example.manuelperera.filmlistviewer.base.di.components.ApplicationComponent;

import butterknife.ButterKnife;

/**
 * Base fragment which all fragments must extend
 */
public abstract class BaseFragment<T extends BasePresenterInterface> extends Fragment implements View.OnClickListener {

    // Base Fragment TAG, set by its parent
    private final String TAG;
    private CharSequence originalTitle;

    public BaseFragment(String tag) {
        TAG = tag;
    }

    /**
     * @return Fragment's TAG
     */
    public String getTAG() {
        return TAG;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(getLayoutResourceId(), container, false);

        // Bind views
        bindViews(rootView);

        // Inject objects with Dagger 2
        injectDependencies();

        // Init views
        initViews(rootView);

        // Parse extras in order to get the fragments' arguments
        final Bundle extras;
        if (savedInstanceState != null) {
            extras = savedInstanceState;
        } else {
            extras = getArguments();
        }

        if (getPresenter() != null) {
            getPresenter().onCreate(extras);
        }

        rootView.setSoundEffectsEnabled(false);
        rootView.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set activity's title
        if (!getTitleString().isEmpty()) {
            setActivityTitle(getTitleString());
        }

        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    @Override
    public void onDestroy() {
        if (!TextUtils.isEmpty(originalTitle)) {
            setActivityTitle(originalTitle.toString());
        }
        originalTitle = null;

        super.onDestroy();

        if (getPresenter() != null) {
            getPresenter().onDestroy();
        }
        destroy();
    }

    @Override
    public void onDestroyView() {
        // Destroy views
        destroyViews();
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Bundle bundleToSave = getBundleToSave();
        if (bundleToSave != null) {
            if (outState == null) {
                outState = new Bundle();
            }

            outState.putAll(bundleToSave);
        }

        super.onSaveInstanceState(outState);
    }

    /**
     * Replace all the annotated fields with ButterKnife annotations with the proper value
     */
    private void bindViews(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    /**
     * Inject Dagger 2 dependencies
     */
    private void injectDependencies() {
        setupComponent(App.getApp(getActivity()).getApplicationComponent());
    }

    public void setActivityTitle(String title) {
        if (getActivity() == null) {
            return;
        }
        originalTitle = getActivity().getTitle();
        getActivity().setTitle(title);
    }

    /**
     * @return Fragment's title
     */
    protected int getTitle() {
        return 0;
    }

    /**
     * @return Fragment's title
     */
    protected String getTitleString() {
        if (getActivity() == null) {
            return "";
        }

        return getActivity().getTitle().toString();
    }

    @Override
    public void onClick(View v) {
        // Implement when required
    }

    /**
     * Specify the layout of the fragment to be inflated in the {@link BaseFragment#onCreateView(LayoutInflater, ViewGroup, Bundle)}
     */
    protected abstract int getLayoutResourceId();

    /**
     * @return The presenter attached to the fragment. This must extends from {@link com.example.core.base.presenters.BasePresenter}
     */
    protected abstract T getPresenter();

    /**
     * This method will setup the injector and will commit the dependencies injections.
     */
    protected abstract void setupComponent(ApplicationComponent applicationComponent);

    /**
     * Initialize all views for that fragment
     *
     * @param rootView Fragment's root view
     */
    protected abstract void initViews(View rootView);

    /**
     * Destroy all views for that fragment
     */
    protected abstract void destroyViews();

    /**
     * Destroy all objects for that fragment
     */
    protected abstract void destroy();

    public Bundle getBundleToSave() {
        return getArguments();
    }
}
