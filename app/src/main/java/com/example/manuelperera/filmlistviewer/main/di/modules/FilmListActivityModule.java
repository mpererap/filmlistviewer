package com.example.manuelperera.filmlistviewer.main.di.modules;


import com.example.core.main.contracts.FilmListActivityContract;
import com.example.core.main.interactors.ConfigurationInteractor;
import com.example.core.main.presenters.FilmListActivityPresenter;
import com.example.domain.repositories.Repository;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerActivity;
import com.example.manuelperera.filmlistviewer.base.fragments.BaseFragment;
import com.example.manuelperera.filmlistviewer.main.fragments.FilmListFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class FilmListActivityModule {

    private final FilmListActivityContract.View view;

    public FilmListActivityModule(FilmListActivityContract.View view) {
        this.view = view;
    }

    @PerActivity
    @Provides
    FilmListActivityContract.Presenter provideFilmListActivityPresenter(
            final ConfigurationInteractor configurationInteractor) {
        return new FilmListActivityPresenter(view, configurationInteractor);
    }

    @PerActivity
    @Provides
    BaseFragment provideFilmListFragment() {
        return new FilmListFragment();
    }
}
