package com.example.manuelperera.filmlistviewer.base.helpers;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.domain.model.ImageConfiguration;

import java.util.List;

public class ImageLoader {

    private ImageConfiguration imageConfiguration;

    public ImageLoader() {
    }

    public void setImageConfiguration(ImageConfiguration imageConfiguration) {
        this.imageConfiguration = imageConfiguration;
    }

    public void displayImage(ImageView imageView, DisplayImageSettings displaySettings,
                             Context context) {
        if (imageView != null && displaySettings != null) {

            // Set the URL of the image that should be loaded into this view and specify the
            // ImageLoader that will be used to make the presentationRequested. For the way the app works right
            // now, local uri's should have higher priority than the url's, so we will only show
            // the last ones if there are not uri's in displayImageSettings.
            if (displaySettings.hasLocalImageBitmap()) {
                loadLocalImage(imageView, displaySettings, context);
            } else {
                String url = buildImageUrl(displaySettings.getImageUrl());
                loadWithUrl(imageView, url, displaySettings, context);
            }
        }
    }

    private String buildImageUrl(String imageUrl) {
        if (imageConfiguration == null) {
            return imageUrl;
        }

        String baseUrl = imageConfiguration.getBaseUrl();
        baseUrl = baseUrl == null ? "" : baseUrl;

        List<String> posterSizes = imageConfiguration.getPosterSizes();
        String posterSize = posterSizes != null && posterSizes.size() > 0 ? posterSizes.get(0) : "";

        if (posterSize.isEmpty() && !baseUrl.isEmpty()) {
            imageUrl = imageUrl.replaceFirst("/", "");
        }

        return baseUrl + posterSize + imageUrl;
    }

    private void loadLocalImage(ImageView imageView, DisplayImageSettings displaySettings,
                                Context context) {
        GlideApp.with(context)
                .load(displaySettings.getLocalImageBitmap())
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(displaySettings.getLoadingResourceId())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(false)
                .into(imageView);
    }

    private void loadWithUrl(ImageView imageView, String imageUrl,
                             DisplayImageSettings displaySettings, Context context) {

        GlideApp.with(context)
                .asBitmap()
                .load(imageUrl)
                .transition(BitmapTransitionOptions.withCrossFade())
                .placeholder(displaySettings.getLoadingResourceId())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(false)
                .into(new BitmapImageViewTarget(imageView) {

                    @Override
                    protected void setResource(Bitmap resource) {
                        if (displaySettings.isCircularImage()) {
                            loadRoundedBitmap(context, resource, imageView);
                        } else {
                            super.setResource(resource);
                        }
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        displaySettings.executeErrorCallback();
                    }
                });
    }

    private static void loadRoundedBitmap(Context context, Bitmap bitmap, ImageView imageView) {

        RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
        circularBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(circularBitmapDrawable);
    }
}


