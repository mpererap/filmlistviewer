package com.example.manuelperera.filmlistviewer.base.di.modules;


import android.content.Context;
import android.support.annotation.Nullable;

import com.example.core.main.interactors.ConfigurationInteractor;
import com.example.domain.datasources.api.ApiClient;
import com.example.domain.datasources.api.ApiDataSource;
import com.example.domain.model.Configuration;
import com.example.domain.repositories.AppRepository;
import com.example.domain.repositories.Repository;
import com.example.manuelperera.filmlistviewer.BuildConfig;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerActivity;
import com.example.manuelperera.filmlistviewer.base.di.scopes.PerApplication;
import com.example.utils.ConnectivityHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @PerApplication
    Repository provideRepository(ApiDataSource apiDataSource) {
        return new AppRepository(apiDataSource);
    }

    @Provides
    @PerApplication
    ApiDataSource provideApiDataSource(ApiClient apiClient, ConnectivityHelper connectivityHelper) {
        return new ApiDataSource(apiClient, connectivityHelper);
    }

    @Provides
    @PerApplication
    ApiClient provideApiClient(Context context) {
        return new ApiClient(context, BuildConfig.FLAVOR_TAG, true);
    }

    @PerApplication
    @Provides
    ConfigurationInteractor provideConfigurationInteractor(final Repository repository) {
        return new ConfigurationInteractor(repository);
    }
}

