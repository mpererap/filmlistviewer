package com.example.manuelperera.filmlistviewer.viewholders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.domain.model.Film;
import com.example.manuelperera.filmlistviewer.R;
import com.example.manuelperera.filmlistviewer.base.helpers.DisplayImageSettings;
import com.example.manuelperera.filmlistviewer.base.helpers.ImageLoader;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.image)
    protected ImageView imageView;
    @BindView(R.id.title)
    protected TextView titleView;
    @BindView(R.id.date)
    protected TextView dateView;
    @BindView(R.id.overview)
    protected TextView overviewView;

    private ImageLoader imageLoader;

    public FilmViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    public void setValue(final Film film, ImageLoader imageLoader) {

        this.imageLoader = imageLoader;

        setTitle(film);
        setDate(film);
        setOverview(film);
        setImage(film);
    }

    private void setTitle(Film film) {
        if (titleView == null) {
            return;
        }

        String title = film == null ? "" : film.getTitle();

        titleView.setText(title);
    }

    private void setDate(Film film) {
        if (dateView == null) {
            return;
        }

        String date = film == null ? "" : film.getReleaseDate();

        date = date == null ? "" : date.split("-")[0];

        dateView.setText(date);
    }

    private void setOverview(Film film) {
        if (overviewView == null) {
            return;
        }

        String overview = film == null ? "" : film.getOverview();

        overviewView.setText(overview);
    }

    private void setImage(Film film) {
        if (imageView == null || imageLoader == null || itemView == null) {
            return;
        }

        String imageUrl = film == null ? "" : film.getPosterPath();

        DisplayImageSettings imageSettings = new DisplayImageSettings();

        imageSettings.setImageUrl(imageUrl);

        imageLoader.displayImage(imageView, imageSettings, itemView.getContext());
    }
}
