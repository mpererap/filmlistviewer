package com.example.domain.repositories;


import com.example.domain.model.Configuration;
import com.example.domain.model.FilmList;

import rx.Observable;

public interface Repository {

    // FILMS
    Observable<FilmList> searchFilm(final int page, final String searchTerm);

    //POPULAR
    Observable<FilmList> getPopularFilms(int page);

    // CONFIGURATION
    Observable<Configuration> getConfiguration();
}
