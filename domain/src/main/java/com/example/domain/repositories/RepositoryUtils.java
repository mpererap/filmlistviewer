package com.example.domain.repositories;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RepositoryUtils {

    /**
     * Cancel an API call
     * @param subscriptions Subscription to be cancelled
     */
    public static void cancel(Subscription... subscriptions) {
        if(subscriptions == null) {
            return;
        }
        for(Subscription subscription : subscriptions) {
            if(subscription == null || subscription.isUnsubscribed()) {
                continue;
            }
            subscription.unsubscribe();
        }
    }

    /**
     * Get subscription from a retrofit observable object
     * @param call Retrofit2 API call
     * @param repositorySubscriber Api RxJava subscriber
     * @return rxJava subscription
     */
    public static Subscription getSubscription(Observable<?> call,
                                               RepositorySubscriber repositorySubscriber) {
        if(call == null) {
            return null;
        }
        return call.subscribeOn(
                Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(
                        AndroidSchedulers.mainThread()) // optional if we want the subscription to be called on ui thread
                .subscribe(repositorySubscriber);
    }
}
