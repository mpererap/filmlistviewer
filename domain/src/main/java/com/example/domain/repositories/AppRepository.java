package com.example.domain.repositories;


import com.example.domain.datasources.api.ApiDataSource;
import com.example.domain.model.Configuration;
import com.example.domain.model.FilmList;

import rx.Observable;

public class AppRepository implements Repository {

    private final ApiDataSource apiDataSource;

    public AppRepository(final ApiDataSource apiDataSource) {
        this.apiDataSource = apiDataSource;
    }

    @Override
    public Observable<FilmList> searchFilm(final int page, final String searchTerm) {
        return apiDataSource.searchFilm(page, searchTerm);
    }

    @Override
    public Observable<FilmList> getPopularFilms(final int page) {
        return apiDataSource.getPopularFilms(page);
    }

    @Override
    public Observable<Configuration> getConfiguration() {
        return apiDataSource.getConfiguration();
    }
}
