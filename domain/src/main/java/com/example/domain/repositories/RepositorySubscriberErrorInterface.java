package com.example.domain.repositories;


import com.example.domain.model.ErrorDetails;

/**
 * API Subscriber error interface required to handle an API error
 */
public interface RepositorySubscriberErrorInterface {

    void onError(ErrorDetails errorDetails);
}
