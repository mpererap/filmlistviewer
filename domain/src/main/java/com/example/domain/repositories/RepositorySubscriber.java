package com.example.domain.repositories;


import com.example.domain.model.ErrorDetails;

import retrofit2.HttpException;
import retrofit2.Response;
import rx.Subscriber;

/**
 * Custom Repository Subscriber in order to handle the errors
 */
public abstract class RepositorySubscriber<T> extends Subscriber<T> implements RepositorySubscriberErrorInterface {

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        if(e instanceof HttpException) {
            onError(parseError(((HttpException) e).response()));
        } else {
            onError(new ErrorDetails());
        }
    }

    private ErrorDetails parseError(Response<?> response) {
        if(response == null || response.isSuccessful()) {
            return null;
        }

        return new ErrorDetails(response.code(), response.message(), response.errorBody());
    }
}

