package com.example.domain.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilmList {

    @SerializedName("page")
    @Expose(serialize = false)
    private int page;

    @SerializedName("results")
    @Expose(serialize = false)
    private List<Film> results;

    @SerializedName("total_results")
    @Expose(serialize = false)
    private int totalResults;

    @SerializedName("total_pages")
    @Expose(serialize = false)
    private int totalPages;

    public FilmList() {

    }

    public int getPage() {
        return page;
    }

    public List<Film> getResults() {
        return results;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }
}
