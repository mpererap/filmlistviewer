package com.example.domain.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Configuration {

    @SerializedName("images")
    @Expose(serialize = false)
    private ImageConfiguration imageConfiguration;

    @SerializedName("change_keys")
    @Expose(serialize = false)
    private List<String> changeKeys;

    public Configuration() {

    }

    public ImageConfiguration getImageConfiguration() {
        return imageConfiguration;
    }

    public List<String> getChangeKeys() {
        return changeKeys;
    }
}
