package com.example.domain.model;


public class NoNetworkConnectivityException extends Exception {

    public static final int ERROR_CODE = -9871;
}
