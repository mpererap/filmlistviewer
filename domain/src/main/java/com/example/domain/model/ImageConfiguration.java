package com.example.domain.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImageConfiguration {

    @SerializedName("base_url")
    @Expose(serialize = false)
    private String baseUrl;

    @SerializedName("secure_base_url")
    @Expose(serialize = false)
    private String secureBaseUrl;

    @SerializedName("backdrop_sizes")
    @Expose(serialize = false)
    private List<String> backdropSizes;

    @SerializedName("logo_sizes")
    @Expose(serialize = false)
    private List<String> logoSizes;

    @SerializedName("poster_sizes")
    @Expose(serialize = false)
    private List<String> posterSizes;

    @SerializedName("profile_sizes")
    @Expose(serialize = false)
    private List<String> profileSizes;

    @SerializedName("still_sizes")
    @Expose(serialize = false)
    private List<String> stillSizes;

    public ImageConfiguration() {

    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getSecureBaseUrl() {
        return secureBaseUrl;
    }

    public List<String> getBackdropSizes() {
        return backdropSizes;
    }

    public List<String> getLogoSizes() {
        return logoSizes;
    }

    public List<String> getPosterSizes() {
        return posterSizes;
    }

    public List<String> getProfileSizes() {
        return profileSizes;
    }

    public List<String> getStillSizes() {
        return stillSizes;
    }
}
