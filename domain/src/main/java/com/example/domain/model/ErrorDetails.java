package com.example.domain.model;


import java.io.IOException;

import okhttp3.ResponseBody;

/**
 * Error object
 */
public class ErrorDetails {

    private final int code;
    private final String message;
    private String errorBodyJson;

    public ErrorDetails() {
        this(-1, null, null);
    }

    public ErrorDetails(int code, String message, ResponseBody errorBody) {
        this.code = code;
        this.message = message;
        if (errorBody != null) {
            try {
                this.errorBodyJson = errorBody.string();
            } catch (IOException ex) {
                this.errorBodyJson = null;
            }
        }
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorBodyJson() {
        return errorBodyJson;
    }
}
