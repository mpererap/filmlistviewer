package com.example.domain.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Film {

    @SerializedName("poster_path")
    @Expose(serialize = false)
    private String posterPath;

    @SerializedName("adult")
    @Expose(serialize = false)
    private boolean adult;

    @SerializedName("overview")
    @Expose(serialize = false)
    private String overview;

    @SerializedName("release_date")
    @Expose(serialize = false)
    private String releaseDate;

    @SerializedName("genre_ids")
    @Expose(serialize = false)
    private List<Integer> genreIds;

    @SerializedName("id")
    @Expose(serialize = false)
    private int id;

    @SerializedName("original_title")
    @Expose(serialize = false)
    private String originalTitle;

    @SerializedName("original_language")
    @Expose(serialize = false)
    private String originalLanguage;

    @SerializedName("title")
    @Expose(serialize = false)
    private String title;

    @SerializedName("backdrop_path")
    @Expose(serialize = false)
    private String backdropPath;

    @SerializedName("popularity")
    @Expose(serialize = false)
    private Float popularity;

    @SerializedName("vote_count")
    @Expose(serialize = false)
    private int voteCount;

    @SerializedName("video")
    @Expose(serialize = false)
    private boolean video;

    @SerializedName("vote_average")
    @Expose(serialize = false)
    private Float voteAverage;

    @Expose(serialize = false,
            deserialize = false)
    private int listPage;

    public Film() {

    }

    public String getPosterPath() {
        return posterPath;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public int getId() {
        return id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public String getTitle() {
        return title;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public Float getPopularity() {
        return popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public boolean isVideo() {
        return video;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public int getListPage() {
        return listPage;
    }

    public void setListPage(int listPage) {
        this.listPage = listPage;
    }
}
