package com.example.domain.datasources.api;

import com.example.domain.model.Configuration;
import com.example.domain.model.FilmList;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    // SEARCH

    @GET(ApiUrls.SEARCH_URL)
    Observable<FilmList> searchFilm(@Query("page") final int page,
                                    @Query("query") final String searchTerms,
                                    @Query("api_key") final String apiKey);

    // POPULAR

    @GET(ApiUrls.POPULAR_URL)
    Observable<FilmList> getPopularFilms(@Query("page") final int page,
                                         @Query("api_key") final String apiKey);

    // CONFIGURATION

    @GET(ApiUrls.CONFIGURATION_URL)
    Observable<Configuration> getConfiguration(@Query("api_key") final String apiKey);
}
