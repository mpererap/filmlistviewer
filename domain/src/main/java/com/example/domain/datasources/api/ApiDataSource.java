package com.example.domain.datasources.api;


import com.example.domain.model.Configuration;
import com.example.domain.model.FilmList;
import com.example.domain.model.NoNetworkConnectivityException;
import com.example.utils.ConnectivityHelper;

import rx.Observable;

public class ApiDataSource {

    private final ApiService apiService;
    private final ConnectivityHelper connectivityHelper;

    public ApiDataSource(ApiClient apiClient, ConnectivityHelper connectivityHelper) {
        this.apiService = apiClient.getService();
        this.connectivityHelper = connectivityHelper;
    }

    private <T> Observable<T> getInterceptedErrorObservable(Observable<T> observable) {
        return observable.onErrorResumeNext(throwable -> {
            if (checkNetworkError()) {
                return Observable.error(new NoNetworkConnectivityException());
            }
            return Observable.error(throwable);
        });
    }

    private boolean checkNetworkError() {
        return connectivityHelper != null && !connectivityHelper.isNetworkAvailable();
    }

    // FILMS

    public Observable<FilmList> searchFilm(final int page, final String searchTerm) {
        return getInterceptedErrorObservable(apiService.searchFilm(page, searchTerm, ApiUrls.API_KEY));
    }

    // POPULAR

    public Observable<FilmList> getPopularFilms(final int page) {
        return getInterceptedErrorObservable(apiService.getPopularFilms(page, ApiUrls.API_KEY));
    }

    // CONFIGURATION

    public Observable<Configuration> getConfiguration() {
        return getInterceptedErrorObservable(apiService.getConfiguration(ApiUrls.API_KEY));
    }
}
