package com.example.domain.datasources.api.interceptors;


import android.content.Context;

import com.example.domain.datasources.api.ApiUrls;
import com.example.utils.ConnectivityHelper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Cache control interceptor so the cache must be used in case of a lack of connection
 */
public class CacheControlInterceptor implements Interceptor {

    // 7 days cache
    private static final int STALE_CACHE_TIME = 60 * 60 * 24 * 7;

    // 2 day cache
    private static final int CONFIGURATION_CACHE_TIME = 60 * 60 * 24 * 2;

    // 1 minute cache
    private static final int RESPONSE_CACHE_TIME = 60;

    private final Context context;

    public CacheControlInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        if (request.method().equals("GET")) {
            if (ConnectivityHelper.isNetworkAvailable(context)
                    && request.url().encodedPath().contains(ApiUrls.CONFIGURATION)) {
                request = request.newBuilder().header("Cache-Control",
                        "public, max-age=" + CONFIGURATION_CACHE_TIME).build();

            } else if (ConnectivityHelper.isNetworkAvailable(context)) {
                request = request.newBuilder().header("Cache-Control",
                        "public, max-age=" + RESPONSE_CACHE_TIME).build();

            } else {
                request = request.newBuilder().header("Cache-Control",
                        "public, only-if-cached, max-stale=" + STALE_CACHE_TIME).build();
            }
        }
        return chain.proceed(request);
    }
}
