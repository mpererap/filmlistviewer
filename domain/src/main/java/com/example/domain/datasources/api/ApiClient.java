package com.example.domain.datasources.api;


import android.content.Context;

import com.example.domain.datasources.api.interceptors.CacheControlInterceptor;
import com.example.domain.repositories.Environment;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.lang.reflect.Type;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class ApiClient {

    private final Context context;
    private final boolean isDebug;
    private final ApiUrls apiUrls;

    // Cache
    private static final int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private static final String API_CACHE_DIR_NAME = "film_list_viewer_api_cache";
    private Cache cache;

    /**
     * Initialize API Client
     *
     * @param context Application's context
     * @param isDebug True whether the app is in debug mode
     */
    public ApiClient(Context context, Environment.Flavor flavor, boolean isDebug) {
        setupApiCache(context);
        this.context = context;
        this.isDebug = isDebug;
        this.apiUrls = new ApiUrls(flavor);
    }

    private void setupApiCache(Context context) {
        if (cache != null) {
            return;
        }
        final File httpCacheDirectory = getApiCacheDirectory(context);
        if (httpCacheDirectory != null) {
            cache = new Cache(httpCacheDirectory, CACHE_SIZE);
        }
    }

    private File getApiCacheDirectory(Context context) {
        if (context == null) {
            return null;
        }
        return new File(context.getCacheDir(), API_CACHE_DIR_NAME);
    }

    /**
     * Creates a retrofit service with its own configuration
     *
     * @param typeAdapterClass Class used on type adapter to convert model to gson
     * @param typeAdapters     Array of custom type adapters
     * @return Created retrofit client object
     */
    private Retrofit getRetrofit(Type typeAdapterClass, Object... typeAdapters) {
        final OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        if (cache != null) {
            httpClientBuilder.cache(cache);
        }

        // Add cache interceptor
        httpClientBuilder.addInterceptor(new CacheControlInterceptor(context));

        if (isDebug) {
            // Add logging. Must be last interceptor to be added in order to be able to see the
            // result of other interceptors
            final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(logging);
        }

        final GsonBuilder gsonBuilder = new GsonBuilder()
                .enableComplexMapKeySerialization()
                .excludeFieldsWithoutExposeAnnotation();
        if (typeAdapters != null && typeAdapterClass != null) {
            for (Object typeAdapter : typeAdapters) {
                gsonBuilder.registerTypeAdapter(typeAdapterClass, typeAdapter);
            }
        }

        return new Retrofit.Builder()
                .baseUrl(apiUrls.getApiBaseUrl())
                .client(httpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .addCallAdapterFactory(
                        RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    /**
     * Creates a retrofit service with its own configuration
     *
     * @return Created retrofit client object
     */
    private Retrofit getRetrofit() {
        return getRetrofit(null);
    }

    /**
     * Creates and get a new retrofit API service object
     *
     * @return Created retrofit API service
     */
    public ApiService getService() {
        return getRetrofit().create(ApiService.class);
    }
}
