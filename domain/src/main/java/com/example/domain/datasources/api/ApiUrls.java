package com.example.domain.datasources.api;


import com.example.domain.repositories.Environment;

public class ApiUrls {

    private final Environment.Flavor flavor;

    ApiUrls(Environment.Flavor flavor) {
        this.flavor = flavor;
    }

    private static final String APP_BASE_DOMAIN = "api.themoviedb.org";
    private static final String APP_TEST_BASE_DOMAIN = "api.themoviedb.org";
    private final static String API_VERSION = "3";
    private final static String BASE_PATH = "/" + API_VERSION + "/";

    final static String API_KEY = "93aea0c77bc168d8bbce3918cefefa45";

    // SEARCH
    final static String SEARCH_URL = BASE_PATH + "search/movie";

    // POPULAR
    final static String POPULAR_URL = BASE_PATH + "movie/popular";

    // CONFIGURATION
    public final static String CONFIGURATION = "configuration";
    final static String CONFIGURATION_URL = BASE_PATH + CONFIGURATION;

    private String getDomain() {
        switch (flavor) {
            case PROD:
                return APP_BASE_DOMAIN;
            case TEST:
                return APP_TEST_BASE_DOMAIN;
        }
        return "";
    }

    private String getProtocol() {
        return "https://";
    }

    String getApiBaseUrl() {
        return getProtocol() + getDomain();
    }
}

