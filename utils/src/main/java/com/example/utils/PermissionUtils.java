package com.example.utils;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;

public class PermissionUtils {

    public enum ManifestPermission {
        ACCESS_NETWORK_STATE(Manifest.permission.ACCESS_NETWORK_STATE);

        private final String value;

        public String getValue() {
            return value;
        }

        ManifestPermission(String value) {
            this.value = value;
        }
    }

    /**
     * Checks if the app has the desired permissions.
     *
     * @param context    Context
     * @param permission Manifest permission
     * @return True if app has permissions, false otherwise.
     */
    public static boolean hasPermission(final Context context,
                                        final ManifestPermission permission) {
        return ContextCompat.checkSelfPermission(context, permission.getValue())
                == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Checks if the app should ask for permissions with a rationale dialog.
     *
     * @param activity   Activity asking for permissions
     * @param permission Manifest permission
     * @return True if should ask with rationale. False otherwise.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean shouldShowRationaleRequest(final Activity activity,
                                                     final ManifestPermission permission) {
        return activity.shouldShowRequestPermissionRationale(permission.getValue());
    }

    /**
     * Checks if the app should ask for permissions with a rationale dialog.
     *
     * @param fragment   Fragment asking for permissions
     * @param permission Manifest permission
     * @return True if should ask with rationale. False otherwise.
     */
    public static boolean shouldShowRationaleRequest(final Fragment fragment,
                                                     final ManifestPermission permission) {
        return fragment.shouldShowRequestPermissionRationale(permission.getValue());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void requestRationalePermission(final Activity activity,
                                                  final ManifestPermission permission, final int requestCode,
                                                  final String rationaleMessage, final String acceptText, final String cancelText,
                                                  final View.OnClickListener onCancelListener) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(rationaleMessage);
        builder.setPositiveButton(acceptText,
                (dialog, which) -> requestPermissions(activity, permission, requestCode));
        builder.setNegativeButton(cancelText, (dialog, which) -> {
            if (onCancelListener != null) {
                onCancelListener.onClick(null);
            }
            dialog.dismiss();
        });
        builder.create().show();
    }

    public static void requestRationalePermission(final Fragment fragment,
                                                  final ManifestPermission permission, final int requestCode,
                                                  final String rationaleMessage, final String acceptText, final String cancelText) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getContext());
        builder.setMessage(rationaleMessage);
        builder.setPositiveButton(acceptText,
                (dialog, which) -> requestPermissions(fragment, permission, requestCode));
        builder.setNegativeButton(cancelText, (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void requestPermission(final Activity activity,
                                         final ManifestPermission permission, final int requestCode) {
        activity.requestPermissions(new String[]{permission.getValue()}, requestCode);
    }

    public static void requestPermission(final Fragment fragment,
                                         final ManifestPermission permission, final int requestCode) {
        fragment.requestPermissions(new String[]{permission.getValue()}, requestCode);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private static void requestPermissions(final Activity activity,
                                           final ManifestPermission permission, final int requestCode) {
        activity.requestPermissions(new String[]{permission.getValue()}, requestCode);
    }

    private static void requestPermissions(final Fragment fragment,
                                           final ManifestPermission permission, final int requestCode) {
        fragment.requestPermissions(new String[]{permission.getValue()}, requestCode);
    }
}
