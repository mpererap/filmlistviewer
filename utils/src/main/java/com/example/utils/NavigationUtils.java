package com.example.utils;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Methods to handle navigation between activities and fragments
 */
public class NavigationUtils {

    /**
     * Navigate to a new fragment
     *
     * @param isAdded         True whether the new fragment must be added or replaced
     * @param fragmentManager Fragment manager that must manage the transaction.
     * @param frameResource   Frame where the fragment must be contained
     * @param fragment        New fragment to be shown
     * @param tag             New fragment's tag
     * @param arguments       New fragment arguments
     */
    public static void navigateToFragment(final boolean isAdded, final FragmentManager fragmentManager,
                                          final int frameResource, final Fragment fragment, final String tag,
                                          final Bundle arguments) {
        navigateToFragment(isAdded, fragmentManager, frameResource, fragment, tag, arguments, -1,
                -1);
    }

    /**
     * Navigate to a new fragment
     *
     * @param isAdded         True whether the new fragment must be added or replaced
     * @param fragmentManager Fragment manager that must manage the transaction.
     * @param frameResource   Frame where the fragment must be contained
     * @param fragment        New fragment to be shown
     * @param tag             New fragment's tag
     * @param arguments       New fragment arguments
     * @param inAnim          Replace in animation
     * @param outAnim         Replace Out animation
     */
    public static void navigateToFragment(final boolean isAdded, final FragmentManager fragmentManager,
                                          final int frameResource, final Fragment fragment, final String tag,
                                          final Bundle arguments, final int inAnim, final int outAnim) {
        if (fragmentManager == null || fragment == null) {
            return;
        }
        if (arguments != null) {
            fragment.setArguments(arguments);
        }
        final FragmentTransaction ft = fragmentManager.beginTransaction();

        if (isAdded) {
            ft.add(frameResource, fragment, tag)
                    .addToBackStack(tag);
        } else {
            ft.replace(frameResource, fragment, tag);
        }

        if (inAnim != -1 && outAnim != -1) {
            ft.setCustomAnimations(inAnim, outAnim);
        }

        ft.commitAllowingStateLoss();
    }
}
